<%-- 
    Document   : registration
    Author     : Timur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<hr>
<div id="main">
    <section>
        <drug>
            <h1>Регистрация</h1>
            <div class="text-drug">
                <form method="POST" action="registration">
                    <p>
                        <label for="login">Логин</label>
                        <input type="text" name="login" id="login"/>
                    </p>
                    <p>
                        <label for="email">E-Mail</label>
                        <input type="email" name="email" id="email"/>
                    </p>
                    <p>
                        <label for="password">Пароль</label>
                        <input type="password" name="password" id="password"/>

                        <label for="password2">Повторите пароль</label>
                        <input type="password" name="password2" id="password2"/>
                    </p>
                    <p>
                        <button type="submit">Зарегистрироваться</button>
                    </p>
                </form>
            </div>
        </drug>
    </section>
</div>
<hr>
